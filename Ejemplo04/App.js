import React, { useState, useEffect } from 'react';
import { View, FlatList, Image, Text, StyleSheet, Platform } from 'react-native';
import axios from 'axios';

const App = () => {
  const [pets, setPets] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get('https://api.thedogapi.com/v1/images');
        setPets(response.data);
      } catch (error) {
        console.error("Error al obtener datos de la API", error);
      }
    };

    fetchData();
  }, []);

  return (
    <View style={styles.container}>
      <FlatList
        data={pets}
        keyExtractor={(item) => item.id.toString()}
        renderItem={({ item }) => (
          <View style={styles.card}>
            <Image source={{ uri: item.url }} style={styles.image} />
            <Text style={styles.text}>Raza: {item.breeds?.[0]?.name || 'Desconocida'}</Text>
          </View>
        )}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F0F0F0', // Color de fondo suave para la pantalla completa
  },
  card: {
    backgroundColor: 'white', // Fondo blanco para cada tarjeta
    borderRadius: 10, // Bordes redondeados para las tarjetas
    marginHorizontal: 10, // Margen horizontal para las tarjetas
    marginVertical: 6, // Espacio vertical entre las tarjetas
    shadowColor: '#000', // Color de la sombra
    shadowOffset: { width: 0, height: 2 }, // Desplazamiento de la sombra
    shadowOpacity: 0.2, // Opacidad de la sombra
    shadowRadius: 4, // Radio de difuminado de la sombra
    elevation: 3, // Elevación para Android
    flexDirection: 'row', // Organizar imagen y texto en fila
    alignItems: 'center', // Alinear ítems en el centro verticalmente
    padding: 10, // Padding para todo el contenido de la tarjeta
  },
  image: {
    width: 100, // Ancho fijo para la imagen
    height: 100, // Alto fijo para la imagen
    borderRadius: 20, // Hacer la imagen completamente redonda
    marginRight: 20, // Margen derecho para separar imagen del texto
  },
  text: {
    flex: 1, // Tomar todo el espacio horizontal disponible
    fontSize: 16, // Tamaño de fuente para el texto
    color: '#333333', // Color oscuro para el texto para mayor legibilidad
    fontWeight: 'bold', // Negrita para el texto de la raza
  },
});

export default App;
