import React, { useState } from 'react';
import { View, Image, TextInput,Text, StyleSheet,TouchableOpacity,Alert} from 'react-native';

const App = () => {
  const [name, setName] = useState('');
  const [lastname, setLastname] = useState('');
  const [ci, setCi] = useState('');

  const handleSubmit = () => {
    // Muestra un Alert simple con la información del formulario
    Alert.alert(
      "Confirmación de Envío",
      `Hola amigos`,      
      [
        { text: "OK" } // Botón para cerrar el Alert
      ]
    );
  };

  return (
    <View style={styles.container}>
       <View style={styles.card}>
        {/* Imagen centrada */}
        <Image
          source={{ uri: 'https://th.bing.com/th/id/OIP.hqk3kKdq4B3T4gTn8KCcQQHaHa?pid=ImgDet&rs=1' }}
          style={styles.roundImage}
        />

        {/* TextInput para el nombre y apellidos */}
        <Text style={styles.label}>Nombres</Text>
        <TextInput
          value={name}
          onChangeText={setName}
          placeholder="Nombres"
          style={styles.input}
        />
        <Text style={styles.label}>Apellidos</Text>
        <TextInput
          value={lastname}
          onChangeText={setLastname}
          placeholder="Apellidos"
          style={styles.input}
        />
        <Text style={styles.label}>CI</Text>
        <TextInput
          value={ci}
          onChangeText={setCi}
          placeholder="CI"
          style={styles.input}
        />
        
      </View>
      <TouchableOpacity style={styles.button} onPress={handleSubmit}>
        <Text style={styles.buttonText}>Enviar Formulario</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#dfc1b9', 
  },
  input: {
    height: 40,
    width: '80%',
    marginVertical: 10, // Espacio vertical entre los inputs
    borderWidth: 1,
    borderColor: '#7e4b44', 
    backgroundColor: '#dfc1b9',
    borderRadius: 20, // Bordes redondeados
    paddingHorizontal: 10, 
    fontSize: 18,
  },
  card: {
    width: '80%', // Ancho del 'card'
    alignItems: 'center', 
    backgroundColor: '#ffe5d9', // Fondo café suave
    borderRadius: 15, // Bordes redondeados del 'card'
    borderColor: '#4b0000', // Color del borde
    borderWidth: 5, // Ancho del borde
    borderBottomWidth: 10, // Ancho del borde inferior más grueso
    paddingTop: 75, // Espacio superior dentro del 'card'
    paddingBottom: 20, // Espacio inferior dentro del 'card'
    marginTop: -75, // Desplaza el 'card' hacia arriba para que la imagen parezca salir de él
  },
  roundImage: {
    width: 150,
    height: 150,
    borderRadius: 75,
    position: 'absolute', // Posición absoluta para sacarla del flujo normal
    top: -75, // Desplaza la imagen hacia arriba para que esté sobre el borde del 'card'
    borderWidth: 3,
    borderColor: '#ffe9e0', 
  },
  label: {
    marginLeft: 20,
    marginTop: 10,
    marginBottom: 5,
    fontSize: 16,
  },
  button: {
    height: 50,
    width: '80%', 
    backgroundColor: '#4b0000', // Color café oscuro para el botón
    borderRadius: 15, // Bordes redondeados para el botón
    justifyContent: 'center', // Centra el texto del botón verticalmente
    alignItems: 'center', // Centra el texto del botón horizontalmente
    marginTop: 20, // Espacio desde el último elemento
    marginHorizontal: 20, // Margen horizontal para mantener la alineación con las cajas de texto
  },
  buttonText: {
    fontSize: 18,
    color: '#FFFFFF', // Texto blanco para contraste
    fontWeight: 'bold', // Texto en negrita
    
  },
});

export default App;